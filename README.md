# Script to download omics data

This script aims at helping handling the downloading of genomics data from UK10K. 

## Historic

I wrote this script while I was post doc in the department of developmental biology. 
I first wrote this script on Windows and plan to rewrite for Linux users.

## Just a single Command

Please run scripttomanageUK10K.cmd

## Authors

* **Emmanuel Sapin** 