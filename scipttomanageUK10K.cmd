@ECHO OFF 
SETLOCAL ENABLEEXTENSIONS ENABLEDELAYEDEXPANSION

REM *************************************************************************
REM *  Script to download, decrypt, and handle the genomic Data from UK10K  *
REM *************************************************************************
ECHO:
ECHO ************************************************************************
ECHO *                            Welcome user !                             *
ECHO *          Let me assist you in managing the UK10K genomic data !       *
ECHO ************************************************************************
ECHO:

REM *******************************
REM *  Check if %1 is a dataset   *
REM *******************************
IF  -%1-==-- (	
	:argument1wrong
	echo Argument 1 must be a dataset to download
	echo Argument 2 must be a folder where to download argument 1
	echo Argument 3 specifies the type of files to download:
	echo     "bam" or "BAM" for bam files, 
	echo     "vcf" or "VCF" for vcf files, 
	echo     and anything else for both !
	goto :END
) 
echo Dataset %1 will be managed
java -jar EgaDemoClient.jar -pf login.txt -lfd %1 > command1.txt
FIND "Access not Permitted" command1.txt > nul
if ERRORLEVEL 1 (
	echo Dataset %1 available
) ELSE (
	goto :argument1wrong
)

REM *************************
REM *  Check %2 is a path   *
REM *************************
IF  -%2-==-- (	
	echo Argument 2 must be a folder where to download %1
	echo Argument 3 specifies the type of files to download
	echo     "bam" or "BAM" for bam files, 
	echo     "vcf" or "VCF" for vcf files, 
	echo     and anything else for both !
	goto :END
)
IF EXIST %2 (
echo Folder %2 exist
) ELSE (
echo Folder %2 does not exist
echo Folder %2 will be created
)
echo Dataset %1 will be downloaded in the folder %2

REM ****************************************
REM *  Download argument 1 in argument 2   *
REM ****************************************
:start_check
java -jar EgaDemoClient.jar -pf login.txt -lr >  command2.txt
FIND "%1" command2.txt > nul & IF NOT ERRORLEVEL 1 ( 
	echo As there is a request which label is %1, this request will be downloaded in the folder %2.
	set somethingtodownload=1
) ELSE (
	echo Dataset %1 not requested yet	
	IF exist %2\*.cip (
		echo these is^(are^) cip file^(s^) in %2
		echo The files in folder %2 and the files of the dataset %1 will be compared
		set somethingtodownload=0
		REM    compare files in argument 2 anf files in argument 1
		java -jar EgaDemoClient.jar -pf login.txt -lfd %1 >command3.txt
		:start_compare
		set /p command3=<command3.txt>nul
		echo Analyzing: "!command3!"
		set first1=!command3:~0,1!
		if "!first1!"=="_" (
			GOTO :end_check
		)
		rem echo !command3!
		set subtring=!command3:EGAR=!
		rem echo !subtring!
		IF !subtring!==!command3! (
			set subtring=!command3:EGAZ=!
			rem echo !subtring!
			IF !subtring!==!command3! (
				set subtring=!command3:EXIT=!
				IF not !subtring!==!command3! (
					GOTO :end_check
				)
				REM remove line
				:remove_line
				MORE /E +1 command3.txt > command3remove1.txt
				DEL command3.txt > nul
				RENAME command3remove1.txt command3.txt > nul
				GOTO :start_compare
			) ELSE (
				echo the line !command3! is refering to a VCF file
				IF NOT "%3"=="bam" ( IF NOT "%3"=="BAM" (
					GOTO :checkfile
				) )
				GOTO :remove_line
			)
		) ELSE (
			echo the line "!command3!" is refering to a BAM file
			REM BAM file found
		    IF NOT "%3"=="vcf" ( IF NOT "%3"=="VCF" (
				:checkfile
				set first3=!command3:~0,3!
				IF not !first3!==EGA ( if not !first3!==SRA (
					set command3=!command3:~1!
					GOTO :checkfile
				) )
				:loop2
				set last3=!command3:~-3!
				IF not !last3!==gpg (
					set command3=!command3:~0,-1!
					GOTO :loop2
				)
				set command3=!command3:/=_!
				set command3=!command3:~0,-3!
				set command3=_!command3!cip
				set namefile=!command3!
				set pathnamefile=%2/!command3!
				if exist !pathnamefile!	(
					echo !pathnamefile! exist
					echo This file won t be added to the request %1
					goto :remove_line
				) ELSE (
					echo !pathnamefile! does not exist
					echo This file is added to the request %1
					set /p command3=<command3.txt>nul
					:findIDfile
					set first4=!command3:~0,4!
					IF not !first4!==EGAF (
						set command3=!command3:~1!
						GOTO :findIDfile
					)
					:loop3
					set last1=!command3:~-1!
					IF not "!last1!"==" " IF not "!last1!"=="e" IF not "!last1!"=="l" IF not "!last1!"=="b" IF not "!last1!"=="a" IF not "!last1!"=="i" IF not "!last1!"=="v" (
						GOTO :PASS
					)
					set command3=!command3:~0,-1!
					GOTO :loop3
					:PASS
					set idfile=!command3!
					echo This file ID is !idfile!
					PING localhost -n 5
					java -jar EgaDemoClient.jar -pf login.txt -rf !idfile! -re abc -label %1
					somethingtodownload=1
					GOTO :remove_line
				)	
			) ) ELSE  (	
				GOTO :remove_line
			)
		)
	) ELSE (
		echo As folder %2 is empty, the full dataset %1 will be requested.
		java -jar EgaDemoClient.jar -pf login.txt -rfd %1 -re abc -label %1
	)
)
:end_check
Echo comparison end
REM check if the request exist now
if !somethingtodownload!==1 ( 	
	java -jar EgaDemoClient.jar -pf login.txt -lr >  command2.txt
	FIND "%1" command2.txt > nul & IF NOT ERRORLEVEL 1 ( 
		REM download the request named argument 1
		:start_download
		rem pause
		cd %2
		java -jar EgaDemoClient.jar -pf login.txt -dr %1 -nt 15
		cd ..
		GOTO :start_check
	) ELSE (
		GOTO :start_check
	)
)
echo The files in %2 and the files in dataset %1 now match
:end_download

